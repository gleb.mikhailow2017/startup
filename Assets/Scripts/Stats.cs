using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stats : MonoBehaviour
{
    int timeto;

    public Timer tm;

    public int hp, food, sleep, Hdelta, Fdelta, Sdelta, hpmax, sleepmax, foodmax;
    public Image Hp, Food, Sleep;

    public GameObject SleepChoose;

    public RectTransform Sspace;
    public Image[] Snums;
    int sleepHour;
    public Animator show;

    private void Start()
    {
        SleepChoose.SetActive(false);
        tm = FindObjectOfType<Timer>();
        replace((FindObjectOfType<Starter>().statsMultipl[1] - 1) * 12);

    }

    public void StatUpdate(Image Fill, int stat)
    {
        Fill.fillAmount = ((((float)stat * 4)-1)/59);
    }

    public void SleepCh()
    {
        SleepChoose.SetActive(!SleepChoose.activeInHierarchy);
        sleepHour = 1;

        string a = sleepHour.ToString();

        if (Sspace.GetComponent<RectTransform>().childCount > a.Length)
        {
            for (int i = 0; i <= Sspace.GetComponent<RectTransform>().childCount - a.Length; i++)
            {
                GameObject c = Sspace.GetComponent<RectTransform>().GetChild(0).gameObject;
                c.GetComponent<RectTransform>().SetParent(null);
                Destroy(c);
            }
        }

        if (Sspace.GetComponent<RectTransform>().childCount < a.Length)
        {
            for (int i = 0; i <= a.Length - Sspace.GetComponent<RectTransform>().childCount; i++)
            {
                GameObject.Instantiate(Snums[0], Sspace.GetComponent<RectTransform>());
            }
        }

        for (int i = 0; i < Sspace.GetComponent<RectTransform>().childCount; i++)
        {
            Sspace.GetComponent<RectTransform>().GetChild(i).GetComponent<Image>().sprite = Snums[int.Parse(a[i].ToString())].sprite;
        }
    }

    public void Choose(int plus)
    {
        if ((plus > 0 && sleepHour < 12) || (plus < 0 && sleepHour > 1))
            sleepHour += plus;

        string a = sleepHour.ToString();

        if (Sspace.GetComponent<RectTransform>().childCount > a.Length)
        {
            for (int i = 0; i <= Sspace.GetComponent<RectTransform>().childCount - a.Length; i++)
            {
                GameObject c = Sspace.GetComponent<RectTransform>().GetChild(0).gameObject;
                c.GetComponent<RectTransform>().SetParent(null);
                Destroy(c);
            }
        }

        if (Sspace.GetComponent<RectTransform>().childCount < a.Length)
        {
            for (int i = 0; i <= a.Length - Sspace.GetComponent<RectTransform>().childCount; i++)
            {
                GameObject.Instantiate(Snums[0], Sspace.GetComponent<RectTransform>());
            }
        }

        for (int i = 0; i < Sspace.GetComponent<RectTransform>().childCount; i++)
        {
            Sspace.GetComponent<RectTransform>().GetChild(i).GetComponent<Image>().sprite = Snums[int.Parse(a[i].ToString())].sprite;
        }
    }

    public void Sleeping()
    {
        if (tm.going) tm.Pause();
        SleepChoose.SetActive(false);

        tm.h += sleepHour;
        if (tm.h > 11)
        {
            if (tm.night) tm.DayUpdate();
            else show.SetTrigger("Show");
            tm.h -= 12;
            tm.night = !tm.night;
            
        }
        tm.FailOut();
        tm.OClock();

        sleep += sleepHour / 2;
        if (sleep > 15) sleep = 15;

        hp += sleepHour / 4;
        if (hp > 15) hp = 15;

        food -= sleepHour / 3;
    }

    private void Update()
    {
        if(Hdelta != hp)
        {
            StatUpdate(Hp, hp);
            Hdelta = hp;
        }

        if (Fdelta != food)
        {
            StatUpdate(Food, food);
            Fdelta = food;
        }

        if (Sdelta != sleep)
        {
            StatUpdate(Sleep, sleep);
            Sdelta = sleep;
        }
    }

    public void replace(int much)
    {
        timeto+=much;
        while (timeto >= 3)
        {
            timeto -= 3;
            food--;
            sleep--;
        }
    }

}
