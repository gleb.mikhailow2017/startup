using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkSet : MonoBehaviour
{
    public GameObject[] prefs;
    public Button[] butprefs;
    public int[] indx;
    public RectTransform[] bSp;
    public Papers pp;
    public bool newDay;

    private void Start()
    {
        pp = FindObjectOfType<Papers>();
        set();
    }

    public void UnPaper()
    {
        if(GetComponent<RectTransform>().childCount == 1)
        {
            pp.PaperOut();
        }
    }
    public void set()
    {
        for (int i = indx.Length - 1; i >= 1; i--)
        {
            int j = Random.Range(0, i);
            int temp = indx[j];
            indx[j] = indx[i];
            indx[i] = temp;
        }

        for (int i = 0; i < bSp.Length; i++)
        {

            WorkParent w = GameObject.Instantiate(prefs[indx[i]], GetComponent<RectTransform>()).GetComponent<WorkParent>();
            w.Click = GameObject.Instantiate(butprefs[indx[i]], bSp[i]).GetComponent<Button>();
            w.Click.GetComponent<WorkButton>().wp = w;
            w.workSet = this;
            if (GetComponent<RectTransform>().childCount > 3)
            {
                if(bSp[i].childCount > 1)
                Destroy(bSp[i].GetChild(0).gameObject);
                Destroy(GetComponent<RectTransform>().GetChild(GetComponent<RectTransform>().childCount - 1).gameObject);
            }
        }
    }
}
