using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Work2 : WorkParent
{
    public Wires[] wires;
    public RectTransform[] exits, exitsSp;
    bool reset = false;

    private void Awake()
    {
        Shaffle();
    }
    public void Shaffle()
    {
        int[] t = new int[exits.Length];
        for (int i = 0; i < exits.Length; i++)
        {
            t[i] = i;
        }

        for (int i = exits.Length-1; i >=1; i--)
        {
            int j = Random.Range(0, i);
            int temp = t[j];
            t[j] = t[i];
            t[i] = temp;
        }

        for (int i = 0; i < exits.Length; i++)
        {
            exits[i].parent = exitsSp[t[i]];
            exits[i].localPosition = new Vector2(0, 0);
        }
    }

    public void WireConnect()
    {
        int t = 0;
        for(int i = 0; i < wires.Length; i++)
        {
            if (wires[i].Done) t ++;
        }

        if(t == wires.Length)
        {
            Money = 1;
            StopWork();
        }
    }
}
