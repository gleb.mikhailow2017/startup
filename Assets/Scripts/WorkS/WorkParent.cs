using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkParent : MonoBehaviour
{
    public bool working;
    public int Money, moneyMultiplier, statsMultipl;
    public Timer timer;
    public Money mn;
    public Papers pp;
    public Stats st;
    public WorkSet workSet;
    public RectTransform Void, ClickVoid;

    public Button Click;

    public float t, i, timeskip;
    private void Start()
    {
        st = FindObjectOfType<Stats>();
        mn = FindObjectOfType<Money>();
        pp = FindObjectOfType<Papers>();
        timer = FindObjectOfType<Timer>();
        gameObject.SetActive(false);
    }

    public void startWork()
    {
        gameObject.SetActive(true);
        working = true;
        i = 0;
        timer.going = false;
        
    }

    public void StopWork()
    {
        i = 0;
        working = false;

        timer.h += timeskip;
        if (timer.h > 11)
        {
            if (timer.night) timer.DayUpdate();
            else timer.going = true;
            timer.h -= 12;
            timer.night = !timer.night;
        }

        timer.FailOut();
        timer.OClock();
        Destroy(Click.gameObject);
        mn.money += Money * moneyMultiplier;
        st.replace(statsMultipl);
        pp.PaperEjected();
        workSet.UnPaper();
        Destroy(gameObject);
    }

}
