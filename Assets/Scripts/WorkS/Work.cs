using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Work : WorkParent
{
    public Sprite[] showel;
    public Image Showel;
    [SerializeField] bool isUp;


    private void Update()
    {
        if (working && i < t)
        {
            i += Time.deltaTime;
        }
        if(i >= t)
        {
            StopWork();
        }
    }

    public void ShowelUse(bool a)
    {
        if (isUp == a)
        {
            Money++;
            if (a) Showel.sprite = showel[0];
            else Showel.sprite = showel[1];
            isUp = !isUp;
        }
    }
}
