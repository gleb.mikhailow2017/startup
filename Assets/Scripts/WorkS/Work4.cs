using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Work4 : WorkParent
{
    public RectTransform[] Spawns;
    public GameObject[] items;
    int ct;

    private void Awake()
    {
        Set();
    }
    public void Set()
    {
        ct = 0;
    }

    public void Plus()
    {
        Money++;
    }

    private void Update()
    {
        
        if (working && i < t)
        {
            i += Time.deltaTime;
        }
        if((int)i != ct)
        {
            ct = (int)i;
            GameObject.Instantiate(items[Random.Range(0, items.Length - 1)], 
                Spawns[Random.Range(0, Spawns.Length - 1)])
                .GetComponent<RectTransform>().localPosition = new Vector2(0,0);
        }
        if (i >= t)
        {
            StopWork();
        }
        
    }
}
