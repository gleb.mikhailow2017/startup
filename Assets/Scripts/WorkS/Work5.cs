using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Work5 : WorkParent
{
    public bool Washing;
    public Image dirt;
    [SerializeField] int tmp;

    private void Awake()
    {
        Set();
    }
    public void Set()
    {
        tmp = 0;
        dirt.color = new Color(1,1,1,1);
    }
    private void Update()
    {
        if (working && i < t)
        {
            i += Time.deltaTime;
        }
        if (i >= t)
        {
            i = 0;
            if (Washing)
            {
                tmp++;
                Wash();
            }
        }
    }
    public void Wash()
    {
        dirt.color = new Color(1,1,1,1-((float)tmp/10));
        if(dirt.color.a <= 0)
        {
            Money = 1;
            StopWork();
        }
    }
}
