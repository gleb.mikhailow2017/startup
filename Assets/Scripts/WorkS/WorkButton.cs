using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkButton : MonoBehaviour
{
    public WorkParent wp;
    public Work3 w3;
    public Papers pp;
    public Money mn;
    public int cost;
    public void WP()
    {
        wp.startWork();
    }

    public void W3S()
    {
        w3.Stop();
        Destroy(gameObject);
    }

    public void Up()
    {
        pp.PowerUp();
    }

    public void BuyWork()
    {
        if (cost <= mn.money)
        {
            mn.money -= cost;
            GetComponentInParent<Lock>().UnLock();
        }
    }
}
