using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coffee : NewWorkParrent
{
    public bool[] Ingridients, Check;
    public Image[] ingr, See;
    public Image[] Cups, CheckCups;
    public int Orders, ord, CheckCup, cup;
    public GameObject Client;

    public RectTransform Sp;
    public Sprite[] nums;

    private void Update()
    {
        if (working && i < t)
        {
            i += Time.deltaTime;
        }
        if (i >= t)
        {
            i = 0;
            working = false;
            NewOrder();
        }
        if(timer.h > 8 && !timer.night && !Worked)
        {
            Yes();
        }
    }

    public void StartWorkA()
    {
        Sp.GetChild(0).gameObject.SetActive(true);
        Sp.GetChild(1).gameObject.SetActive(true);
        if (Orders - ord < 10) Sp.GetChild(0).gameObject.SetActive(false);
        Sp.GetChild(0).GetComponent<Image>().sprite = nums[(Orders - ord) % 10];
        Sp.GetChild(1).GetComponent<Image>().sprite = nums[(Orders - ord) / 10];
    }
    public void Yes()
    {
        Nope.SetActive(false);
    }
    public void NewOrder()
    {
        Client.SetActive(true);
        CheckCup = Random.Range(0, 3);
        for (int i = 0; i < Check.Length; i++)
        {
            int r = Random.Range(0, 2);
            if (r == 1)
            {
                Check[i] = false;
                See[i].gameObject.SetActive(false);
            }
            else
            {
                Check[i] = true;
                See[i].gameObject.SetActive(true);
            }

            if (CheckCup == i)
                CheckCups[i].gameObject.SetActive(true);
            else
                CheckCups[i].gameObject.SetActive(false);
        }
    }

    private void Awake()
    {
        ord = 0;
        for (int i = 0; i < ingr.Length; i++)
        {
            ingr[i].gameObject.SetActive(Ingridients[i]);
        }
        Client.SetActive(false);
    }
    public void Select(int i)
    {
        ingr[i].gameObject.SetActive(!ingr[i].gameObject.activeInHierarchy);
        Ingridients[i] = !Ingridients[i];
    }

    public void SelectCup(int i)
    {
        cup = i;
        for(int i1 = 0; i1 < Cups.Length; i1++)
        {
            if(i1 == i)
            {
                Cups[i].color = new Color(0.8f, 0.8f, 0.8f, 1);
            }
            else
            {
                Cups[i1].color = new Color(1, 1, 1, 1);
            }
        }
    }

    public void Finish()
    {
        int b = 0;
        for(int i = 0; i < Check.Length; i++)
        {
            if (Check[i] == Ingridients[i]) b++;
        }

        if(b == Check.Length && CheckCup == cup)
        {
            Money++;
        }
        Client.SetActive(false);
        CheckCups[CheckCup].gameObject.SetActive(false);
        working = true;

        ord++;

        if (ord == Orders)
            StopWork();


        if (Orders - ord < 10) Sp.GetChild(0).gameObject.SetActive(false);
        Sp.GetChild(0).GetComponent<Image>().sprite = nums[(Orders-ord)/10];
        Sp.GetChild(1).GetComponent<Image>().sprite = nums[(Orders - ord) % 10];
    }
}
