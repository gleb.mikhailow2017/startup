using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Work3 : WorkParent
{
    public RectTransform[] PrblSp;
    public GameObject Problem;
    int tmp;
    public int Probs;
    public int[] indx;


    private void Awake()
    {
        Set();
    }
    public void Set()
    {
        tmp = 0;
        for (int i = indx.Length - 1; i >= 1; i--)
        {
            int j = Random.Range(0, i);
            int temp = indx[j];
            indx[j] = indx[i];
            indx[i] = temp;
        }

        for (int i = 0; i < Probs; i++)
        {
            WorkButton w = GameObject.Instantiate(Problem, PrblSp[indx[i]]).GetComponent<WorkButton>();
            w.GetComponent<RectTransform>().localPosition = new Vector2(0, 0);
            w.w3 = this;
        }
    }
    public void Stop()
    {
        tmp++;
        if(tmp == Probs)
        {
            Money = 1;
            StopWork();
        }
    }
}
