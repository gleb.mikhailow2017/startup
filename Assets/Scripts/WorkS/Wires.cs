using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Wires : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public int color;

    public bool dragOnSurfaces = true;

    public RectTransform Exit;
    public bool Done;

    [SerializeField] GameObject m_DraggingIcon;
    [SerializeField] RectTransform m_DraggingPlane;

    public GameObject wire;

    public void Reset1()
    {
        Destroy(GetComponent<RectTransform>().GetChild(0).gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!Done) 
        {
            var canvas = GetComponentInParent<Canvas>().gameObject;
            if (canvas == null)
                return;


            // We have clicked something that can be dragged.
            // What we want to do is create an icon for this.
            m_DraggingIcon = new GameObject("icon");
            wire = new GameObject("wire");

            wire.transform.SetParent(canvas.transform, false);
            wire.transform.SetAsLastSibling();

            m_DraggingIcon.transform.SetParent(canvas.transform, false);
            m_DraggingIcon.transform.SetAsLastSibling();

            var image = m_DraggingIcon.AddComponent<Image>();

            var wir = wire.AddComponent<Image>();

            wir.sprite = Exit.GetComponent<Image>().sprite;
            wir.SetNativeSize();

            image.sprite = GetComponent<Image>().sprite;
            image.SetNativeSize();

            if (dragOnSurfaces)
                m_DraggingPlane = transform as RectTransform;
            else
                m_DraggingPlane = canvas.transform as RectTransform;

            SetDraggedPosition(eventData);
        } 
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(!Done)
            SetDraggedPosition(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!Done) 
        {
            if ((m_DraggingIcon.GetComponent<RectTransform>().position - Exit.position).magnitude * 667.39 < 15)
            {
                Done = true;

                GameObject w = GameObject.Instantiate(wire, gameObject.GetComponent<RectTransform>());
                w.transform.position = wire.transform.position;

                FindObjectOfType<Work2>().WireConnect();
            }

            if (m_DraggingIcon != null)
                Destroy(m_DraggingIcon);

            if (wire != null)
                Destroy(wire);
        }
    }

    static public T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        Transform t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }


    private void SetDraggedPosition(PointerEventData data)
    {
        if (dragOnSurfaces && data.pointerEnter != null && data.pointerEnter.transform as RectTransform != null)
            m_DraggingPlane = data.pointerEnter.transform as RectTransform;

        var rt = m_DraggingIcon.GetComponent<RectTransform>();
        var wr = wire.GetComponent<RectTransform>();

        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlane, data.position, data.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
            rt.rotation = m_DraggingPlane.rotation;
            rt.sizeDelta = new Vector2(GetComponent<RectTransform>().rect.width, GetComponent<RectTransform>().rect.height);

            

            wr.position = (GetComponent<RectTransform>().position + rt.position) / 2;
            wr.sizeDelta = new Vector2(10, (rt.position - GetComponent<RectTransform>().position).magnitude * 667.39f);
            if(rt.position.x - GetComponent<RectTransform>().position.x > 0)
                wr.rotation = Quaternion.Euler(0,0,Vector2.Angle(rt.position - GetComponent<RectTransform>().position , Vector2.down));
            else
                wr.rotation = Quaternion.Euler(0, 0, -Vector2.Angle(rt.position - GetComponent<RectTransform>().position, Vector2.down));
        }

    }
}
