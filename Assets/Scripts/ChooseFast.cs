using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseFast : MonoBehaviour
{
    public RectTransform Front, Back, Fast, Good;

    public void Choose(bool fast)
    {
        if (fast)
        {
            Fast.SetParent(Front);
            Good.SetParent(Back);
        }
        else
        {
            Good.SetParent(Front);
            Fast.SetParent(Back);
        }
    }
}
