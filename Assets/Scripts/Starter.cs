using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Starter : MonoBehaviour
{
    public GameObject Main, Selecter;
    public RectTransform Diffic;
    public int[] statsMultipl;

    int tmpInt;
    int tmpSt;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        Selecter.SetActive(false);
    }
    public void StartGame()
    {
        SceneManager.LoadScene(1);
        /*
        FindObjectOfType<Money>().money = ((statsMultipl[0] - 1) * 15) + 5;
        FindObjectOfType<Stats>().replace((statsMultipl[1]-1) * 6);
        foodid[] fdd = FindObjectsOfType<foodid>();
        for(int f = 0; f < Mathf.Abs(statsMultipl[2]-4); f++)
        {
            int r = Random.Range(0, fdd.Length);
            FindObjectOfType<Money>().money += fdd[r].cost;
            fdd[r].Get();
        }
        */
    }

    public void Select()
    {
        Main.SetActive(false);
        Selecter.SetActive(true);
        DiffUpdate(1, 1);
    }
    public void SelecterStat()
    {
        statsMultipl[tmpSt] = tmpInt;
        DiffUpdate(tmpInt, tmpSt);
    }

    public void test1(GameObject o)
    {
        o.GetComponent<RectTransform>().localPosition = new Vector2(0, 0); 
    }
    public void test2(int tmp)
    {
        tmpInt = tmp;
    }
    public void test3(int tmp)
    {
        tmpSt = tmp;
    }

    public void DiffUpdate(int a, int stat)
    {
        int c = 0;
        statsMultipl[stat] = a;

        for(int i = 0; i < statsMultipl.Length; i++)
        {
            c += statsMultipl[i];
        }

        float tmp = (float)c;
        if (tmp % 3 > 5)
            c = (c / 3) + 1;
        else
            c = c / 3;

        for (int i = 0; i < Diffic.childCount; i++)
        {
            if (c <= i) Diffic.GetChild(i).gameObject.SetActive(false);
            else Diffic.GetChild(i).gameObject.SetActive(true);
        }
    }
}
