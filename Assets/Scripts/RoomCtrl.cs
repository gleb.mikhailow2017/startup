using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomCtrl : MonoBehaviour
{
    public Button[] B;
    public int room;
    public Transform[] Room;
    public Transform Void, Empty;
    Vector3 nl = new Vector3(0, 0, 0);

    private void Start()
    {
        if (room == 2)
        {
            B[1].gameObject.SetActive(false);
        }
        if (room == 0)
        {
            B[0].gameObject.SetActive(false);
        }

        if (room != 0)
            Room[0].SetParent(Void);
        else
            Room[0].SetParent(Empty);

        if (room != 1)
            Room[1].SetParent(Void);
        else
            Room[1].SetParent(Empty);

        if (room != 2)
            Room[2].SetParent(Void);
        else
            Room[2].SetParent(Empty);

        Room[0].localPosition = nl;
        Room[1].localPosition = nl;
        Room[2].localPosition = nl;
    }

    public void Right()
    {
        if(room == 1)
        {
            B[1].gameObject.SetActive(false);
        }
        else
        {
            B[0].gameObject.SetActive(true);
        }
        Room[room].SetParent(Void);
        Room[room].localPosition = nl;

        room++;

        Room[room].SetParent(Empty);
        Room[room].localPosition = nl;

    }

    public void Left()
    {
        if (room == 1)
        {
            B[0].gameObject.SetActive(false);
        }
        else
        {
            B[1].gameObject.SetActive(true);
        }
        Room[room].SetParent(Void);
        Room[room].localPosition = nl;

        room--;

        Room[room].SetParent(Empty);
        Room[room].localPosition = nl;

    }

}
