using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewWorkParrent : MonoBehaviour
{
    public Timer timer;
    public Money mn;
    public Stats st;

    public GameObject Nope;

    public float t, i;
    public bool working;
    public int Money, MoneyPlus, timeskip;
    public bool Worked;

    public void StartWork()
    {
        gameObject.SetActive(true);
        working = true;
        i = 0;
        timer.going = false;
    }

    private void Start()
    {
        st = FindObjectOfType<Stats>();
        mn = FindObjectOfType<Money>();
        timer = FindObjectOfType<Timer>();
        gameObject.SetActive(false);
    }

    public void StopWork()
    {
        i = 0;
        working = false;
        Worked = true;

        timer.h += timeskip;
        if (timer.h > 11)
        {
            if (timer.night) timer.DayUpdate();
            else timer.going = true;
            timer.h -= 12;
            timer.night = !timer.night;
        }
        Nope.SetActive(true);
        timer.FailOut();
        timer.OClock();
        gameObject.SetActive(false);
        mn.money += Money * MoneyPlus;
    }
}
