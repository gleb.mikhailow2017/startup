using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Papers : MonoBehaviour
{
    public Button paper;
    public Sprite[] mail;
    public GameObject Mail;
    public GameObject PaperList;
    public int level;
    public Button[] ups;
    public Button phone;
    public int[] costs;
    public RectTransform upsSpace;

    public Money mn;


    private void Start()
    {
        mn = FindObjectOfType<Money>();
        PaperList.SetActive(false);
    }
    public void PaperOn(int day)
    {
        Mail.GetComponent<SpriteRenderer>().sprite = mail[1];
        paper.gameObject.SetActive(true);
        if(day == 10 && level == 0)
        {
            GameObject b = GameObject.Instantiate(ups[0].gameObject, upsSpace);
            b.GetComponent<RectTransform>().localPosition = new Vector2(0, 0);
            b.GetComponent<WorkButton>().pp = this;
        }
    }

    public void PowerUp()
    {
        if (mn.money >= costs[level])
        {
            Destroy(upsSpace.GetChild(0).gameObject);
            mn.money -= costs[level];
            level++;
            if (level == 1) phone.gameObject.SetActive(true);
        }
    }

    public void PaperEjected()
    {
        PaperList.SetActive(!PaperList.activeInHierarchy);
    }
    public void PaperOut()
    {
        Mail.GetComponent<SpriteRenderer>().sprite = mail[0];
        paper.gameObject.SetActive(false);
    }
}
