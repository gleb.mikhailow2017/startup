using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Money : MonoBehaviour
{
    public Image[] Numbers;
    public int money, deltaMoney;
    public GameObject space;
    bool moneystop;

    private void Start()
    {
        money = (Mathf.Abs(FindObjectOfType<Starter>().statsMultipl[0] - 4) * 15) + 5;
    }
    private void Update()
    {
        if (deltaMoney != money && money < 999999)
            MoneyCount();
        else
            money = deltaMoney;
        deltaMoney = money;
    }

    public void MoneyCount()
    {
        string a = money.ToString();

        if (space.GetComponent<RectTransform>().childCount > a.Length)
        {
            for (int i = 0; i <= space.GetComponent<RectTransform>().childCount - a.Length; i++)
            {
                GameObject c = space.GetComponent<RectTransform>().GetChild(0).gameObject;
                c.GetComponent<RectTransform>().SetParent(null);
                Destroy(c);
            }
        }

        if (space.GetComponent<RectTransform>().childCount < a.Length)
        {
            for (int i = 0; i <= a.Length - space.GetComponent<RectTransform>().childCount; i++)
            {
                GameObject.Instantiate(Numbers[0], space.GetComponent<RectTransform>());
            }
        }

        DopMoneyUpdate();
    }
    public void DopMoneyUpdate()
    {
        string a = money.ToString();

        for (int i = 0; i < space.GetComponent<RectTransform>().childCount; i++)
        {
            space.GetComponent<RectTransform>().GetChild(i).GetComponent<Image>().sprite = Numbers[int.Parse(a[i].ToString())].sprite;
        }
    }
}