using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Image[] Numbers, DayNums;
    public GameObject Hspace, Mspace, NewDayspace;
    public Sprite[] ampm;
    public Image AmPm;

    public int days, timeMult, timeMultConst;

    public float h;
    public float m;
    int delta;

    [SerializeField] Papers pp;

    public Image dark, pause, go, doublego;
    public bool night;

    public bool going;

    Stats st;
    WorkSet ws;
    FoodSet[] fs;

    public Sprite[] arrows, times;
    public GameObject Marrow, Harrow, NewDay;

    public void Pause()
    {
        going = !going;
        if (going) pause.sprite = times[0];
        else pause.sprite = times[1];
    }
    public void Go()
    {
        timeMult = timeMultConst;
        go.sprite = times[3];
        doublego.sprite = times[4];
    }
    public void Double()
    {
        timeMult = timeMultConst*2;
        go.sprite = times[2];
        doublego.sprite = times[5];
    }

    public void FailOut()
    {
        if (going) pause.sprite = times[0];
        else pause.sprite = times[1];
        if (timeMult == timeMultConst)
        {
            go.sprite = times[3];
            doublego.sprite = times[4];
        }
        else
        {
            go.sprite = times[2];
            doublego.sprite = times[5];
        }

        if (!night)
        {
            dark.color = new Color(0, 0, 0, ((90 - (h * 15)) / 255));
            AmPm.sprite = ampm[0];
        }
        else
        {
            dark.color = new Color(0, 0, 0, (((h - 5) * 15) / 255));
            AmPm.sprite = ampm[1];
        }
        delta = (int)(m / 7.5);

        if (h % 3 == 0)
        {
            Harrow.GetComponent<SpriteRenderer>().sprite = arrows[2];
            Harrow.transform.rotation = Quaternion.Euler(0, 0, (((int)h / 3) - 1) * -90);
        }

        if ((h + 1) % 3 == 0)
        {
            Harrow.GetComponent<SpriteRenderer>().sprite = arrows[3];
            Harrow.transform.rotation = Quaternion.Euler(0, 0, (((int)h / 3) - 1) * -90);
        }

        if ((h + 2) % 3 == 0)
        {
            Harrow.GetComponent<SpriteRenderer>().sprite = arrows[4];
            Harrow.transform.rotation = Quaternion.Euler(0, 0, (((int)h / 3) - 1) * -90);
        }

        if ((int)(m / 7.5f) % 2 == 0)
        {
            Marrow.GetComponent<SpriteRenderer>().sprite = arrows[0];
            Marrow.transform.rotation = Quaternion.Euler(0, 0, (int)(m / 7.5f) * -45);

        }
        else
        {
            Marrow.GetComponent<SpriteRenderer>().sprite = arrows[1];
            Marrow.transform.rotation = Quaternion.Euler(0, 0, ((int)(m / 7.5f) - 3) * -45);
        }

        if (!night && (int)h >= 6) pp.PaperOn(days);
        if (night && (int)h >= 2) pp.PaperOut();

        if (!night && (int)h <= 6)
        {
            dark.color = new Color(0, 0, 0, ((90 - (h * 15)) / 255));
        }

        if (night && (int)h >= 6)
        {
            dark.color = new Color(0, 0, 0, (((h - 5) * 15) / 255));
        }
    }

    private void Start()
    {
        timeMult = timeMultConst;
        st = FindObjectOfType<Stats>();
        ws = FindObjectOfType<WorkSet>();
        fs = FindObjectsOfType<FoodSet>();
        FailOut();
    }

    void Update()
    {
        if(going)
        m += Time.deltaTime*timeMult;

        if ((int)(m / 7.5f) > delta) MinuteUpdate();
        if (m >= 60) HourUpdate();
    }

    public void DayUpdate()
    {
        days++;
        going = false;
        pause.sprite = times[1];

        string a = days.ToString();

        NewWorkParrent[] nwp = FindObjectsOfType<NewWorkParrent>();

        for(int i = 0; i < nwp.Length; i++)
        {
            nwp[i].Worked = false;
        }

        if (NewDayspace.GetComponent<RectTransform>().childCount > a.Length)
        {
            GameObject c = NewDayspace.GetComponent<RectTransform>().GetChild(0).gameObject;
            c.GetComponent<RectTransform>().SetParent(null);
            Destroy(c);
            print(0);
        }

        if (NewDayspace.GetComponent<RectTransform>().childCount > a.Length)
        {
            GameObject c = NewDayspace.GetComponent<RectTransform>().GetChild(0).gameObject;
            c.GetComponent<RectTransform>().SetParent(null);
            Destroy(c);
            print(0);
        }

        for (int i = 0; i < a.Length; i++)
        {
            
            if (NewDayspace.GetComponent<RectTransform>().childCount < a.Length)
            {
                GameObject.Instantiate(DayNums[0], NewDayspace.GetComponent<RectTransform>());
            }
            NewDayspace.GetComponent<RectTransform>().GetChild(i).GetComponent<Image>().sprite = DayNums[int.Parse(a[i].ToString())].sprite;
        }

        ws.set();
        for(int i1 = 0; i1 < fs.Length; i1++)
        {
            fs[i1].set();
        }

        NewDay.GetComponent<Animator>().SetTrigger("NewDay");
    }

    public void HourUpdate()
    {
        h++;
        if ((int)h == 12)
        {
            h = 0;
            night = !night;
            if (!night)
            {
                DayUpdate();
            }
        }
        m = 0;
        delta = 0;

        if(!night)
            AmPm.sprite = ampm[0];
        else
            AmPm.sprite = ampm[1];

        if (!night && h <= 6)
        {
            dark.color = new Color(0, 0, 0, ((90 - (h * 15)) / 255));
        }

        if (night && h >= 6)
        {
            dark.color = new Color(0, 0, 0, (((h - 5) * 15) / 255));
        }

        if (!night && (int)h >= 6) pp.PaperOn(days);
        if (night && (int)h >= 2) pp.PaperOut();

        if (h % 3 == 0)
        {
            Harrow.GetComponent<SpriteRenderer>().sprite = arrows[2];
            Harrow.transform.rotation = Quaternion.Euler(0, 0, (((int)h / 3) - 1) * -90);
        }

        if ((h+1) % 3 == 0)
        {
            Harrow.GetComponent<SpriteRenderer>().sprite = arrows[3];
            Harrow.transform.rotation = Quaternion.Euler(0, 0, (((int)h / 3) - 1) * -90);
        }

        if ((h+2) % 3 == 0)
        {
            Harrow.GetComponent<SpriteRenderer>().sprite = arrows[4];
            Harrow.transform.rotation = Quaternion.Euler(0, 0, (((int)h / 3) - 1) * -90);
        }

        OClock();
        MinuteUpdate();
        delta = 0;

        st.replace(1);
    }

    public void OClock()
    {
        string a = ((int)h).ToString();

        if (Hspace.GetComponent<RectTransform>().childCount > a.Length)
        {
            GameObject c = Hspace.GetComponent<RectTransform>().GetChild(0).gameObject;
            c.GetComponent<RectTransform>().SetParent(null);
            Destroy(c);
        }

        if (Hspace.GetComponent<RectTransform>().childCount < a.Length)
        {
            GameObject.Instantiate(Numbers[0], Hspace.GetComponent<RectTransform>());
        }

        if (Hspace.GetComponent<RectTransform>().childCount == a.Length)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Hspace.GetComponent<RectTransform>().GetChild(i).GetComponent<Image>().sprite = Numbers[int.Parse(a[i].ToString())].sprite;
            }
        }
    }

    public void MinuteUpdate()
    {
        if ((int)(m /7.5f)%2 == 0)
        {
            Marrow.GetComponent<SpriteRenderer>().sprite = arrows[0];
            Marrow.transform.rotation = Quaternion.Euler(0, 0, (int)(m / 7.5f) * -45);

            string a = ((int)m).ToString();
            int[] b = new int[a.Length];

            print(a);

            for (int i = 0; i < a.Length; i++)
            {
                b[i] = int.Parse(a[i].ToString());
            }

            for (int i = 0; i < 2; i++)
            {
                if(b.Length!=i)
                    Mspace.GetComponent<RectTransform>().GetChild(i).GetComponent<Image>().sprite = Numbers[b[i]].sprite;
                else
                    Mspace.GetComponent<RectTransform>().GetChild(i).GetComponent<Image>().sprite = Numbers[0].sprite;
            }
        }
        else
        {
            Marrow.GetComponent<SpriteRenderer>().sprite = arrows[1];
            Marrow.transform.rotation = Quaternion.Euler(0, 0, ((int)(m / 7.5f)-3) * -45);
        }
        if(delta == 3)
        {
            if(night) dark.color = new Color(0, 0, 0, (((h - 5.5f) * 15) / 255));
            else dark.color = new Color(0, 0, 0, ((82.5f - (h * 15)) / 255));
        }
        delta++;
    }
}
