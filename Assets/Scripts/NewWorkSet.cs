using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewWorkSet : MonoBehaviour
{
    public GameObject[] work;
    public void Set()
    {
        for(int i = 0; i < work.Length; i++)
        {
            GameObject.Instantiate(work[i], transform);
        }
    }
}
