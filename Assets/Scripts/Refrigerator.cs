using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Refrigerator : MonoBehaviour
{
    public RoomCtrl rc;
    public GameObject[] fridge;
    public int room;

    private void Start()
    {
        for (int i = 0; i < fridge.Length; i++)
        {
            if (rc.room == room)
                fridge[i].SetActive(true);
            else
                fridge[i].SetActive(false);
        }
    }
    public void rUpd()
    {
        for (int i = 0; i < fridge.Length; i++)
        {
            if (rc.room == room)
                fridge[i].SetActive(true);
            else
                fridge[i].SetActive(false);
        }
    }
}
