using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fridge : MonoBehaviour
{
    public Sprite[] states;
    public Button fridge, empty;
    public RectTransform frsp;
    public GameObject FrList;

    public List<Food> all;

    private void Start()
    {
        FrList.SetActive(false);
        fridge.GetComponent<Image>().sprite = states[0];
    }
    public void Opening()
    {
        if (fridge.GetComponent<Image>().sprite != states[0]) fridge.GetComponent<Image>().sprite = states[0];
        else fridge.GetComponent<Image>().sprite = states[1];

        FrList.SetActive(!FrList.activeInHierarchy);

        if (FrList.activeInHierarchy)
        {
            if (all.Count < frsp.childCount)
            {
                for (int i = (frsp.childCount - all.Count); i > 0; i--)
                {
                    Destroy(frsp.GetChild(0));
                }
            }

            if (all.Count > frsp.childCount)
            {
                for (int i = (all.Count - frsp.childCount); i > 0; i--)
                {
                    GameObject.Instantiate(empty, frsp);
                }
            }

            FoodReset();
        }
    }

    public void FoodReset()
    {
        for (int i = 0; i < all.Count; i++)
        {
            frsp.GetChild(i).GetComponent<Food>().count = all[i].count;
            frsp.GetChild(i).GetComponent<Food>().hpheal = all[i].hpheal;
            frsp.GetChild(i).GetComponent<Food>().foodheal = all[i].foodheal;
            frsp.GetChild(i).GetComponent<Food>().icon = all[i].icon;
            frsp.GetChild(i).GetComponent<Food>().visible();
        }
    }
}
