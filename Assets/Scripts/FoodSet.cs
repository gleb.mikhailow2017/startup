using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodSet : MonoBehaviour
{
    public Button[] butprefs;
    public RectTransform[] bSp;
    public int[] indx;

    [SerializeField] bool Main;

    private void Start()
    {
        Main = true;
        set();
    }

    public void set()
    {
        for (int i = indx.Length - 1; i >= 1; i--)
        {
            int j = Random.Range(0, i);
            int temp = indx[j];
            indx[j] = indx[i];
            indx[i] = temp;
        }

        int start = 0;

        for (int i = 0; i < bSp.Length; i++)
        {
            if(bSp[i].childCount > 0)
                Destroy(bSp[i].GetChild(0).gameObject);
            GameObject w = GameObject.Instantiate(butprefs[indx[i]].gameObject, bSp[i]);
            w.GetComponent<RectTransform>().localPosition = new Vector2(0, 0);
            w.GetComponent<foodid>().Starts();

            if (start <= (3 - FindObjectOfType<Starter>().statsMultipl[2]) && Main)
            {
                start++;
                FindObjectOfType<Money>().money += w.GetComponent<foodid>().cost;

                w.GetComponent<foodid>().Get();
            }
        }

        FindObjectOfType<FoodList>().List.SetActive(false);
        Main = false;
    }
}
