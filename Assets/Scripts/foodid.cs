using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class foodid : MonoBehaviour
{
    public int id, hpheal, foodheal;
    public Sprite icon;
    public Image refer;
    [SerializeField] Food empty;
    public int cost;
    public Money mn;

    public RectTransform Emptyspace;

    public void Starts()
    {
        icon = refer.sprite;
        mn = FindObjectOfType<Money>();
        Emptyspace = GameObject.FindGameObjectWithTag("EmptySpace").GetComponent<RectTransform>();
    }

    public void Get()
    {
        if (mn.money >= cost)
        {
            mn.money -= cost;

            bool get = false;

            Fridge tmp = FindObjectOfType<Fridge>();

            for (int i = 0; i < tmp.all.Count; i++)
            {
                if (tmp.all[i].id == id && !get)
                {
                    tmp.all[i].count++;
                    get = true;
                }
            }

            if (!get)
            {
                Food em = GameObject.Instantiate(empty.gameObject, Emptyspace).GetComponent<Food>();
                em.gameObject.transform.localPosition = new Vector3(0, 0, 0);

                em.icon = icon;
                em.hpheal = hpheal;
                em.foodheal = foodheal;
                em.id = id;
                em.count = 1;
                tmp.all.Add(em);
            }
        }
    }
}
