using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : MonoBehaviour
{
    bool anim = false;
    public GameObject cost;
    public void UnLock()
    {
        cost.SetActive(true);
        GetComponent<Animator>().SetTrigger("UnLock");
        anim = true;
    }

    private void Update()
    {
        if (anim && !GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Unlock"))
            Destroy(gameObject);
    }
}
