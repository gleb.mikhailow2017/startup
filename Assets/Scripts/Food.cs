using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Food : foodid
{
    public Stats st;
    public int count;
    public RectTransform Space;
    public Sprite[] num;
    public Image num0;

    public void Eat()
    {
        st.hp += hpheal;
        if (st.hp > st.hpmax) st.hp = st.hpmax;
        if (st.hp < 0) st.hp = 0;

        st.food += foodheal;
        if (st.food > st.foodmax) st.food = st.foodmax;
        if (st.food < 0) st.food = 0;

        count--;
        if (count == 0)
        {
            Destroy(gameObject);
        }
        else visible();
    }

    private void Start()
    {
        st = FindObjectOfType<Stats>();
    }

    public void visible()
    {
        GetComponent<Image>().sprite = icon;
        
        string a = count.ToString();

        if (a.Length < Space.childCount)
        {
            for (int i = (Space.childCount - a.Length ); i > 0; i--)
            {
                Destroy(Space.GetChild(0));
            }
        }

        if (a.Length > Space.childCount)
        {
            for (int i = (a.Length - Space.childCount); i > 0; i--)
            {
                GameObject.Instantiate(num0, Space);
            }
        }

        FoodReset(a);
        
    }
    
    public void FoodReset(string a)
    {
        for (int i = 0; i < Space.childCount; i++)
        {
            Space.GetChild(i).GetComponent<Image>().sprite = num[int.Parse(a[i].ToString())];
        }
    }
    
}
