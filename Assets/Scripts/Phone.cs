using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Phone : MonoBehaviour
{
    public GameObject PhoneUID;
    public GameObject[] Applications;

    private void Start()
    {
        PhoneUID.SetActive(false);
    }
    public void Application(int focus)
    {
        Applications[focus].SetActive(true);
    }
    public void ApplicationQuit()
    {
        for (int i = 0; i < Applications.Length; i++)
        {
            Applications[i].SetActive(false);
        }
    }
    public void UID()
    {
        PhoneUID.SetActive(!PhoneUID.activeInHierarchy);
        if (PhoneUID.activeInHierarchy)
        {
            for (int i = 0; i < Applications.Length; i++)
            {
                Applications[i].SetActive(false);
            }
        }
    }
}
